package ggd.auth.vo;

import java.sql.Timestamp;
import java.util.List;

public class AdmFunc {

	private String funcId;
	
	private String funcName;
	
	private AdmFunc parent;
	
	private boolean isRoot;
	
	private String url;
	
	private Integer sort;
	
	private Timestamp createDate;
	
	private Timestamp updateDate;
	
	private boolean isEnabled;
	
	private boolean isApproved;
	
	
}